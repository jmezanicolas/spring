use inventario_dev;
select * from universidades;
INSERT INTO universidades (id, descripcion, direccion, longitud, rector, telefono, Latitud)  VALUES  ('8646a927-9af6-45f9-a4b3-60beaed94a55',  'UMSNH', 'Morelia, Michoacán', -101.2071186, 'Rigoberto', '4433866311', 19.6902501);
update universidades set 
 descripcion = 'Universidad Michoacana de San Nicolás de Hidalgo' , 
 direccion =  'Gral. Francisco J. Mugica S/N, Ciudad Universitaria, Fraccionamiento Real Universidad, 58030 Morelia, Mich.',
 imagen ='https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Umsnh.jpg/240px-Umsnh.jpg',
 longitud = -101.2071186, 
 rector =  'Dr. Medardo Serna González', 
 telefono = '01 443 322 - 3500', 
 Latitud = 19.6902501
where id = 'd7a436ad-936d-489e-8050-07246f3e615b';

insert into facultades VALUES('64515ad5-d22a-4063-8a73-f2d1d4548e61','Escuela de Ciencias Agropecuarias','Dr. Noé Armando Ávila Ramírez','http://www.cienciasagropecuarias.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('ee3d91eb-7969-4c7f-92aa-e415cd94056a', 'Avenida Mariano Jiménez S/N, Colonia El Varillero, Apatzingán, Michoacán.', 19.0857611, null, '64515ad5-d22a-4063-8a73-f2d1d4548e61', -102.3680596);

insert into facultades VALUES('76726724-bd73-49bb-97f9-b0ad319b1125','Escuela de Enfermería y Salud Pública','Lic. L.E. Ma. de la Luz Sánchez Plaza','http://www.enfermeria.umich.mx','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('65c8b719-a855-44c5-9c75-235e51178230', 'Gertrudis Bocanegra No. 330, Colonia Cuauhtémoc, Morelia, Michoacán, C.P. 58020.', 19.6988541, '(443) 312 2490 #(443) 313 7698', '76726724-bd73-49bb-97f9-b0ad319b1125', -101.1826206);

insert into facultades VALUES('96dc26d1-ee92-4c60-bd48-30dedb8aa0fa','Facultad de Letras','Dr. Juan Carlos González Vidal','http://www.letras.umich.mx','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('89c8b719-a855-44c5-9c75-235e51178230', 'Avenida Madero Oriente No. 580, Colonia Centro, Morelia, Michoacán, C.P. 58000', 19.7027576, null, '96dc26d1-ee92-4c60-bd48-30dedb8aa0fa', -101.1860417);

insert into facultades VALUES('49dc26d1-ee92-4c60-bd48-30dedb8aa0fa','Escuela Popular de Bellas Artes','Lic. Miguel Ángel Villa Álvarez','http://www.artes.umich.mx','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('e808ad12-6e73-4fae-a8b1-8b0e60031d3f', 'Guillermo Prieto No. 87, Colonia Centro, Morelia, Michoacán, C.P. 58000', 19.7036136, '01 443 312 0984', '49dc26d1-ee92-4c60-bd48-30dedb8aa0fa', -101.1945137);
insert into detalles_facultades VALUES('3454e199-4017-4465-9491-bec571f1ff46', 'Edificio A-3, Ciudad Universitaria, Francisco J. Múgica S/N, Colonia Felicitas del Río, Morelia, Michoacán, C.P. 58030.', 19.6903838, null, '49dc26d1-ee92-4c60-bd48-30dedb8aa0fa', -101.2040653);

insert into facultades VALUES('e0a0ffe1-90e0-49fb-8a9e-f7dbcf123cbb','Facultad de Agrobiología "Presidente Juárez"','D.C. Martha Elena Pedraza Santos','http://www.agrobiologia.umich.mx','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('b043351a-7cb2-4844-8fe0-9dd71c427f78', 'Paseo Lázaro Cárdenas esquina Berlín, Colonia Viveros, Uruapan, Michoacán.', 19.3959359, null, 'e0a0ffe1-90e0-49fb-8a9e-f7dbcf123cbb', -102.057796);

insert into facultades VALUES('a0958f13-3685-4d13-baad-42af0828f0f4','Facultad de Arquitectura','Arq. Judith Núñez Aguilar','http://www.arq.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('b043351a-7cb2-4844-8fe0-add71c427f78', 'Nuevo Edificio de Arquitectura, Ciudad Universitaria, Francisco J. Múgica S/N, Colonia Felicitas del Río, Morelia, Michoacán, C.P. 58030', 19.6884751, null, 'a0958f13-3685-4d13-baad-42af0828f0f4', -101.2041239);

insert into facultades VALUES('b6e4344c-d780-4053-b6bb-48487d279876','Facultad de Biología','M.C. Carlos Armando Tena Morelos','http://bios.biologia.umich.mx','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('e529d2cf-d7da-4f81-9634-7a6f4dec3736', 'Edificio R, Planta Baja, Ciudad Universitaria, Francisco J. Múgica S/N, Colonia Felicitas del Río, Morelia, Michoacán, C.P. 58030.', 19.687547, null, 'b6e4344c-d780-4053-b6bb-48487d279876', -101.201766);
	
insert into facultades VALUES('b199e5d8-8b2d-4685-a26d-4365ce998321','Facultad de Ciencias Físico-Matemáticas "Mat. Luis Manuel Rivera Gutiérrez"','Dr. Eduardo Salvador Tututi Hernández','http://www.fismat.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('f529d2af-d1de-4f81-9634-7a6f4dec3736', 'Edificio B, Ciudad Universitaria, Francisco J. Múgica S/N, Colonia Felicitas del Río, C.P. 58030, Morelia, Michoacán.', 19.6878097, 'Teléfono (443) 322 3500 Extensión 1221', 'b199e5d8-8b2d-4685-a26d-4365ce998321', -101.2033652);

insert into facultades VALUES('a6adfaac-6b79-4a47-9b77-9f24dce9ab7a','Facultad de Ciencias Médicas y Biológicas "Dr. Ignacio Chavez"','Dr. Arturo Valencia Ortiz','http://www.medicina.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('f729d2af-a1de-4f81-9634-7a6f4dec3736', 'Avenida Rafael Carrillo y Dr. González Herrejón s/n, Colonia Cuauhtémoc, Morelia, Michoacán, C.P. 58020.', 19.6963258, 'Teléfono (443) 322 3500 Extensión 1221', 'a6adfaac-6b79-4a47-9b77-9f24dce9ab7a', -101.1802583);

insert into facultades VALUES('45a2e333-d7d5-4465-ae90-8571552ee3f0','Facultad de Contaduría y Ciencias Administrativas','Dra. Virginia Hernández Silva','http://www.fcca.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('a529d2cf-d7de-4f81-9634-7a6f4dec3736', 'Edificio A-II, planta baja, Ciudad Universitaria, Francisco J. Múgica S/N, Colonia Felicitas del Río, Morelia, Michoacán, C.P. 58030', 19.6874052, '(443) 322 3500 Extensión 3120', '45a2e333-d7d5-4465-ae90-8571552ee3f0', -101.2016448);

insert into facultades VALUES('3c93d6b3-c30d-461e-93c3-29f88d1234c4','Facultad de Derecho y Ciencias Sociales','Mtro. Damián Arévalo Orozco','http://www.themis.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into detalles_facultades VALUES('ca29d2cf-d7de-4f81-9634-7a6f4dec3736', 'Avenida Tata Vasco No. 200, Centro, Morelia, Michoacán, C.P. 58000', 19.7006974, '(443) 313 1412', '3c93d6b3-c30d-461e-93c3-29f88d1234c4', -101.1776928);

insert into facultades VALUES('bb3ce899-3c6c-4fa6-ab8a-e7eac1758cf0','Facultad de Economía “Vasco de Quiroga”','Dr. Rodrigo Gómez Monge','http://www.economia.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('9e6269a4-6bcb-435a-a23c-ba7eb85b739a','Facultad de Enfermería','D.E. Ana Celia Anguiano Morán','http://www.enfermeria.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('dbfb8b2e-b699-4935-bbb8-d7b4ca826c83','Facultad de Filosofía "Samuel Ramos"','M. F. C. Carlos Alberto Bustamante Penilla','http://filos.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('9278fca3-9675-4ce7-8885-1b365bdec961','Facultad de Historia','Mtra. Tzutzuqui Heredia Pacheco','http://cceh.historia.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('6cacd2b0-3374-4181-962e-378163f468eb','Facultad de Ingeniería Civil','Dr. Wilfrido Martínez Molina','http://www.fic.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('24ef7983-bbba-4156-812c-f15acd1d7833','Facultad de Ingeniería Eléctrica','Dr. Juan Anzures Marín','http://www.fie.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('dfb1c7e6-4fed-4431-873e-c3fdeb3bcddb','Facultad de Ingeniería en Tecnología de la Madera','Dr. José Guadalupe Rutiaga Quiñones','http://www.fitecma.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('d3af283c-3e16-4378-a5c1-840c4b8fa12e','Facultad de Ingeniería Mecánica','Ing. José de Jesús Padilla Gómez','http://www.fim.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('69513425-7ce2-42e7-8f7b-42ba7a47165f','Facultad de Ingeniería Química','Dra. María del Carmen Chávez Parga','http://www.fiq.umich.mx/fiqumsnh/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('851d5cc2-4d5b-4fcf-9e10-0e58b2c093ea','Facultad de Medicina Veterinaria y Zootecnia','Dr. José Luis Solorio Rivera','http://www.vetzoo.umich.mx','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('a2c2ece5-e3cc-4c8e-a449-4659ae6f6648','Facultad de Odontología','C. D. E. O. Alejandro Larios Trujillo','http://www.odontologia.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('aebce1a4-b12c-4e94-974e-d3720021174b','Facultad de Psicología','Dr. Raúl Ernesto García Rodríguez','http://www.psicologia.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');
insert into facultades VALUES('4d64f054-fd6b-4e3b-9ce1-b00f595065af','Facultad de Químico Farmacobiología','M.C. Gabino Estévez Delgado','http://www.qfb.umich.mx/','d7a436ad-936d-489e-8050-07246f3e615b');

select * from detalles_facultades;
select * from facultades;