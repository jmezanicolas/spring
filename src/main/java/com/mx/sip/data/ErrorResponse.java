package com.mx.sip.data;

import com.mx.sip.utils.JsonConverter;
import com.mx.sip.utils.Returnable;

public class ErrorResponse implements Returnable {
	private String message;
	private String detail;

	public ErrorResponse(String message, String detail) {
		super();
		this.message = message;
		this.detail = detail;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		JsonConverter<ErrorResponse> converter = new JsonConverter<ErrorResponse>(this);
		return converter.asJsonSring();
	}

}
