package com.mx.sip.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.mx.sip.utils.Returnable;

@Entity
@Table(name = "imagenes_facultades")
public class ImgFacultades  implements Returnable{
	@Id
	private String id;
	@Column
	private String url;
	@Column
	private String descripcion;
	@OneToOne
	private Facultades facultades;

	public ImgFacultades() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Facultades getFacultades() {
		return facultades;
	}

	public void setFacultades(Facultades facultades) {
		this.facultades = facultades;
	}

}
