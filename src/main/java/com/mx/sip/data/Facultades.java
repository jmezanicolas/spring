package com.mx.sip.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.sip.utils.Returnable;

@Entity
@Table(name = "facultades")
public class Facultades implements Returnable{
	@Id
	private String id;
	@Column
	private String descripcion;
	@Column
	private String director;
	@Column
	private String url;
	@OneToOne
	private Universidades universidad;
	
	public Facultades() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@JsonIgnore
	public Universidades getUniversidad() {
		return universidad;
	}

	public void setUniversidad(Universidades universidad) {
		this.universidad = universidad;
	}
	
	}
