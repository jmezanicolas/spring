package com.mx.sip.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mx.sip.utils.JsonConverter;
import com.mx.sip.utils.Returnable;

@Entity
@Table(name = "universidades")
public class Universidades implements Returnable  {
	@Id
	private String id;
	@Column
	private String descripcion;
	@Column
	private String direccion;
	@Column
	private String rector;
	@Column
	private String telefono;
	@Column
	private Double latitud;
	@Column
	private Double longitud;
	@Column
	private String imagen;

	public Universidades() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getRector() {
		return rector;
	}


	public void setRector(String rector) {
		this.rector = rector;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public Double getLatitud() {
		return latitud;
	}


	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}


	public Double getLongitud() {
		return longitud;
	}


	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}


	public String getImagen() {
		return imagen;
	}


	public void setImagen(String imagen) {
		this.imagen = imagen;
	}


	@Override
	public String toString() {
		JsonConverter<Universidades> converter = new JsonConverter<Universidades>(this);
		return converter.asJsonSring();
	}
}
