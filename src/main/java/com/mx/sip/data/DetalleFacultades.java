package com.mx.sip.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.mx.sip.utils.Returnable;

@Entity
@Table(name = "detalles_facultades")
public class DetalleFacultades  implements Returnable{
	@Id
	private String id;
	@Column
	private String direccion;
	@Column
	private String telefono;
	@Column
	private Double latitud;
	@Column
	private Double Longitud;
	@OneToOne
	private Facultades facultades;

	public DetalleFacultades() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return Longitud;
	}

	public void setLongitud(Double longitud) {
		Longitud = longitud;
	}

	public Facultades getFacultades() {
		return facultades;
	}

	public void setFacultades(Facultades facultades) {
		this.facultades = facultades;
	}



}
