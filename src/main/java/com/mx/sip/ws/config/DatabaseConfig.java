package com.mx.sip.ws.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mx.sip.data.DetalleFacultades;
import com.mx.sip.data.Facultades;
import com.mx.sip.data.ImgFacultades;
import com.mx.sip.data.Universidades;
import com.mx.sip.ws.helper.PropertiesHelper;



@Configuration
@ComponentScan("com.mx")
@EnableTransactionManagement
public class DatabaseConfig {
	
	@Bean(name="dataSource")
	public DataSource getDataSource(){
		BasicDataSource dataSource = new BasicDataSource();
    	dataSource.setDriverClassName(PropertiesHelper.getInstance().getProperty("db.driver"));
    	dataSource.setUrl(PropertiesHelper.getInstance().getProperty("db.url"));
    	dataSource.setUsername(PropertiesHelper.getInstance().getProperty("db.user"));
    	dataSource.setPassword(PropertiesHelper.getInstance().getProperty("db.password"));

    	return dataSource;
	}
	
	private Properties getHibernateProperties(){
    	Properties properties = new Properties();    	

    	properties.put("hibernate.show_sql", PropertiesHelper.getInstance().getProperty("hibernate.show_sql"));
    	properties.put("hibernate.dialect", PropertiesHelper.getInstance().getProperty("hibernate.dialect"));
    	properties.put("hibernate.hbm2ddl.auto", PropertiesHelper.getInstance().getProperty("hbm2ddl.auto"));
    	properties.put("hibernate.hbm2ddl.import_files", PropertiesHelper.getInstance().getProperty("hibernate.hbm2ddl.import_files"));
    	properties.put("hibernate.enable_lazy_load_no_trans", true);	 
    	properties.put("hibernate.connection.CharSet", "utf8");
    	properties.put("hibernate.connection.characterEncoding", "utf8");	        
    	
    	return properties;
    }

    
    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) {
    	LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
    	sessionBuilder.addProperties(getHibernateProperties());	    	
    	sessionBuilder.addAnnotatedClasses(
    			Universidades.class,
    			Facultades.class,
    			DetalleFacultades.class,
    			ImgFacultades.class
    	);
    	return sessionBuilder.buildSessionFactory();
    }
    
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(
			SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(
				sessionFactory);			

		return transactionManager;
	}
    

    
    
//    @Autowired
//    @Bean(name = "userDAO")
//    public UserDAO getUserDao(SessionFactory sessionFactory) {
//    	return new UserDAOImpl(sessionFactory);
//    }
}
