package com.mx.sip.ws.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class PropertiesHelper {
	private static PropertiesHelper instance = null;
	private static Properties properties = null;
	private static final Logger log = LogManager.getLogger(PropertiesHelper.class);
	private static Set<String> allowedEnvironments = null;

	private PropertiesHelper() {
		allowedEnvironments = new HashSet<String>();
		allowedEnvironments.add("local");
		allowedEnvironments.add("dev");
		allowedEnvironments.add("test");
		allowedEnvironments.add("ide");
		load();
	}

	private void load() {
		InputStream stream = null;
		String profile = "";
		String activeProfile = null;
		try {
			activeProfile = null;
			activeProfile = System.getProperty("spring.profiles.active");
			if (activeProfile != null) {
				if (allowedEnvironments.contains(activeProfile)) {
					profile = "-" + activeProfile;
				}
			}
			stream = this.getClass().getClassLoader().getResourceAsStream("application" + profile + ".properties");
			assert(stream != null):
				"No se encontró el archivo propiedades: 'application[-${spring.profiles.active}].properties'";
			properties = new Properties();
			properties.load(stream);
		} catch (Exception e) {
			log.error("No se pudo cargar el archivo de propiedades", e);
		}finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					log.error("No se pudo cerrar archivo de propiedades ", e);
				}
			}
		}
	}
	public static final PropertiesHelper getInstance() {
		if (instance == null) {
			instance = new PropertiesHelper();
		}
		return instance;
	}

	public String getProperty(String key) {
		String property = properties.getProperty(key);
		return property;
	}
}
