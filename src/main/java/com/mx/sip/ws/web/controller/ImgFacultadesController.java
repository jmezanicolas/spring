package com.mx.sip.ws.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.sip.business.ImgFacultadesLogic;
import com.mx.sip.dao.ResponseData;
import com.mx.sip.data.ImgFacultades;
import com.mx.sip.utils.GetMessage;
import com.mx.sip.utils.Returnable;

@Controller
@RestController
@RequestMapping(path = "/imgfacultades")
public class ImgFacultadesController {
	private static final Logger log = LogManager.getLogger(ImgFacultadesController.class);
	@Autowired
	private ImgFacultadesLogic logic;

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public String getData(HttpServletRequest request, HttpServletResponse response) {
		String jsonString = "";
		Map<String, Object> params = new HashMap<>();

		ResponseData<Returnable> responseData = logic.list(params);
		jsonString = responseData.toString();
		return jsonString;
	}

	@RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public String createRegister(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) List<ImgFacultades> imgFacultades) throws IOException {
		ImgFacultades obj = null;
		String msg = null;
		try {
			obj = imgFacultades.get(0);
			String id = logic.create(obj);
			if (id != null) {
				msg = GetMessage.onSuccess("El registro se agrego correctamente");
			} else {
				throw new Exception("No se pudo agregar el registro");
			}
		} catch (Exception e) {
			log.error(e);
			msg = GetMessage.onFailure(e.getMessage());
			return msg;
		}
		return msg;
	}

	@RequestMapping(path = "/update", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public String updateEvent(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) List<ImgFacultades> imgFacultades) throws IOException {
		String msg = null;
		ImgFacultades obj = null;
		try {
			obj = imgFacultades.get(0);
			boolean update = logic.update(obj);
			if (update) {
				msg = GetMessage.onSuccess("El registro se actualizo correctamente");
			} else {
				throw new Exception("No se pudo actualizar el registro");
			}
		} catch (Exception e) {
			log.error(e);
			return GetMessage.onFailure(e.getMessage());
		}
		return msg;
	}

	@RequestMapping(path = "{uuid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String removeEvent(HttpServletRequest request, HttpServletResponse response, @PathVariable String uuid)
			throws IOException {
		String msg = null;
		boolean delete = false;
		try {
			delete = logic.delete(uuid);
			if (delete) {
				msg = GetMessage.onSuccess("El registro se elimino correctamente");
			} else {
				throw new Exception("El registro no se pudo eliminar");
			}
		} catch (Exception e) {
			log.error(e);
			return GetMessage.onFailure(e.getMessage());
		}
		return msg;
	}

	
}
