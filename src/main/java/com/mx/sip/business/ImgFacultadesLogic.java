package com.mx.sip.business;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.sip.dao.GenericLogic;
import com.mx.sip.dao.ResponseData;
import com.mx.sip.dao.ResponseList;
import com.mx.sip.dao.SimpleGenericDao;
import com.mx.sip.data.ImgFacultades;
import com.mx.sip.utils.Returnable;
@Component
public class ImgFacultadesLogic  implements GenericLogic<ImgFacultades> {
	private static final Logger log = LogManager.getLogger(ImgFacultadesLogic.class);
	private SimpleGenericDao<ImgFacultades> dao;
	@Autowired
	private SessionFactory sessionFactory;

	@PostConstruct
	public void init() {
		dao = new SimpleGenericDao<ImgFacultades>(ImgFacultades.class, sessionFactory);
	}

	@Override
	public ImgFacultades findId(String id) {
		ImgFacultades universidad = null;
		try {
			universidad = dao.findId(id);
		} catch (Exception e) {
			log.error(e);
		}
		return universidad;
	}

	@Override
	public String create(ImgFacultades object) {
		String id = null;
		try {
			id = dao.save(object);
		} catch (Exception e) {
			log.error(e);
		}
		return id;
	}

	@Override
	public boolean update(ImgFacultades object) {
		boolean update = false;
		try {
			update = dao.update(object);
		} catch (Exception e) {
			log.error(e);
		}
		return update;
	}

	@Override
	public boolean delete(String id) {
		boolean delete = false;
		try {
			delete = dao.delete(id);
		} catch (Exception e) {
			log.error(e);
		}
		return delete;
	}

	@Override
	public ResponseData<Returnable> list( Map<String, Object> params) {
		ResponseData<Returnable> responseData = null;
		try {
			ResponseList<ImgFacultades> ImgFacultades = dao.list();
			responseData = new ResponseData<Returnable>(ImgFacultades);
		} catch (Exception e) {
			log.error(e);
		}

		return responseData;
	}



}

