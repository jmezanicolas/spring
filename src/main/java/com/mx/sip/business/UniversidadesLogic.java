package com.mx.sip.business;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.sip.dao.GenericLogic;
import com.mx.sip.dao.ResponseData;
import com.mx.sip.dao.ResponseList;
import com.mx.sip.dao.SimpleGenericDao;
import com.mx.sip.data.Universidades;
import com.mx.sip.utils.Returnable;

@Component
public class UniversidadesLogic implements GenericLogic<Universidades> {
	private static final Logger log = LogManager.getLogger(UniversidadesLogic.class);

	private SimpleGenericDao<Universidades> dao;
	@Autowired
	private SessionFactory sessionFactory;

	@PostConstruct
	public void init() {
		loadDao();
	}

	public void loadDao() {
		try {
			dao = new SimpleGenericDao<Universidades>(Universidades.class, sessionFactory);
		} catch (Exception e) {
			log.error(e);
		}

	}

	@Override
	public Universidades findId(String id) {
		Universidades universidad = null;
		try {
			universidad = dao.findId(id);
		} catch (Exception e) {
			log.error(e);
		}
		return universidad;
	}

	@Override
	public String create(Universidades object) {
		String id = null;
		try {
			if (getUniversidades(object)) {
				object.setId(UUID.randomUUID().toString());
				id = dao.save(object);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return id;
	}

	private boolean getUniversidades(Universidades object) {
		boolean noExiste = false;
		Map<String, Object> params = new HashMap<>();
		ResponseList<Universidades> rList;
		params.put("longitud", object.getLongitud());
		params.put("latitud", object.getLatitud());
		rList = dao.list(params);
		if (rList != null) {
			if (rList.size() == 0) {
				noExiste = true;
			}
		} else {
			noExiste = true;
		}
		return noExiste;
	}

	@Override
	public boolean update(Universidades object) {
		boolean update = false;
		try {
			update = dao.update(object);
		} catch (Exception e) {
			log.error(e);
		}
		return update;
	}

	@Override
	public boolean delete(String id) {
		boolean delete = false;
		try {
			delete = dao.delete(id);
		} catch (Exception e) {
			log.error(e);
		}
		return delete;
	}

	@Override
	public ResponseData<Returnable> list(Map<String, Object> params) {
		ResponseData<Returnable> responseData = null;
		try {
			ResponseList<Universidades> universidades;
			universidades = dao.list();
			responseData = new ResponseData<Returnable>(universidades);
		} catch (Exception e) {
			log.error(e);
		}

		return responseData;
	}

}
