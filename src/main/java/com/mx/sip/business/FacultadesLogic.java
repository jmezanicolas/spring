package com.mx.sip.business;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.sip.dao.GenericLogic;
import com.mx.sip.dao.ResponseData;
import com.mx.sip.dao.ResponseList;
import com.mx.sip.dao.SimpleGenericDao;
import com.mx.sip.data.Facultades;
import com.mx.sip.utils.Returnable;

@Component
public class FacultadesLogic implements GenericLogic<Facultades> {
	private static final Logger log = LogManager.getLogger(FacultadesLogic.class);
	private SimpleGenericDao<Facultades> dao;
	@Autowired
	private SessionFactory sessionFactory;

	@PostConstruct
	public void init() {
		dao = new SimpleGenericDao<Facultades>(Facultades.class, sessionFactory);
	}

	@Override
	public Facultades findId(String id) {
		Facultades universidad = null;
		try {
			universidad = dao.findId(id);
		} catch (Exception e) {
			log.error(e);
		}
		return universidad;
	}

	@Override
	public String create(Facultades object) {
		String id = null;
		try {
			id = dao.save(object);
		} catch (Exception e) {
			log.error(e);
		}
		return id;
	}

	@Override
	public boolean update(Facultades object) {
		boolean update = false;
		try {
			update = dao.update(object);
		} catch (Exception e) {
			log.error(e);
		}
		return update;
	}

	@Override
	public boolean delete(String id) {
		boolean delete = false;
		try {
			delete = dao.delete(id);
		} catch (Exception e) {
			log.error(e);
		}
		return delete;
	}

	@Override
	public ResponseData<Returnable> list( Map<String, Object> params) {
		ResponseData<Returnable> responseData = null;
		try {
			ResponseList<Facultades> Facultades = dao.list();
			responseData = new ResponseData<Returnable>(Facultades);
		} catch (Exception e) {
			log.error(e);
		}

		return responseData;
	}

}
