package com.mx.sip.utils;

import com.mx.sip.data.ErrorResponse;

public class GetMessage {

	public static String onSuccess(String detail) {
		ErrorResponse er = new ErrorResponse("Success", detail);
		return er.toString();
	}

	public static String onFailure(String detail) {
		ErrorResponse er = new ErrorResponse("Failure", detail);
		return er.toString();
	}
}
