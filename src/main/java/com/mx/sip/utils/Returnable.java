package com.mx.sip.utils;

public interface Returnable {
	
	default String toJsonString(){
		JsonConverter<Returnable> converter = new JsonConverter<Returnable>(this);
		return converter.asJsonSring();
	}
	
}