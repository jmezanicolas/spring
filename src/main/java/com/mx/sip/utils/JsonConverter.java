package com.mx.sip.utils;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonConverter<T> {

	private static final Logger log = LogManager.getLogger(JsonConverter.class);

	private static ObjectMapper mapper = null;
	private T type = null;

	/**
	 * 
	 */
	public JsonConverter(T type) {
		this.type = type;
	}

	public String asJsonSring() {
		String json = null;
		if (mapper == null) {
			mapper = new ObjectMapper();
		}
		try {
			json = mapper.writeValueAsString(type);
		} catch (JsonProcessingException e) {
			log.error(e);
		}
		return json;
	}

}