package com.mx.sip.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.sip.utils.JsonConverter;

public class Page {

	@JsonIgnore
	private Integer nextPage = -1;

	@JsonIgnore
	private Integer previousPage = -1;
	
	private Integer max= 0;

	@JsonIgnore
	private String path;
	
	@JsonIgnore
	private Long totalItems = 0L;

	public Long getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(Long totalItems) {
		this.totalItems = totalItems;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public Page() {
		path = "";
	}

	public Page(String path) {
		this();
		this.path = path;
	}

	public Page(String path, Integer previousPage, Integer nextPage) {
		this(path);
		this.nextPage = nextPage;
		this.previousPage = previousPage;
	}
	
	public Page(Cursor cursor){
		setNextPage(cursor.getNextPage().intValue());
		setPreviousPage(cursor.getLastPage().intValue());
		setMax(cursor.getTotalPages().intValue());	
		setTotalItems(cursor.getTotalItems());

	}

	/**
	 * @return the previous
	 */
	public String getPrevious() {
		String previous = "";
		if (previousPage > 0) {
			StringBuilder builder = null;
			if (path.contains("?")) {
				builder = new StringBuilder(path).append('&').append("page=").append(previousPage);
				previous = builder.toString();
			}
			else{
				builder = new StringBuilder(path).append('?').append("page=").append(previousPage);
				previous = builder.toString();
			}
		}
		return previous;
	}

	/**
	 * @return the next
	 */
	public String getNext() {
		String next = "";
		if (nextPage > 0) {
			StringBuilder builder = null;
			if (path.contains("?")) {
				builder = new StringBuilder(path).append('&').append("page=").append(nextPage);
				next = builder.toString();
			} else {
				builder = new StringBuilder(path).append('?').append("page=").append(nextPage);
				next = builder.toString();
			}

		}
		return next;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String json = null;
		JsonConverter<Page> converter = new JsonConverter<Page>(this);
		json = converter.asJsonSring();
		return json;
	}

	/**
	 * @return the nextPage
	 */
	@JsonIgnore
	public Integer getNextPage() {
		return nextPage;
	}

	/**
	 * @param nextPage
	 *            the nextPage to set
	 */
	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}

	/**
	 * @return the previousPage
	 */
	@JsonIgnore
	public Integer getPreviousPage() {
		return previousPage;
	}

	/**
	 * @param previousPage
	 *            the previousPage to set
	 */
	public void setPreviousPage(Integer previousPage) {
		this.previousPage = previousPage;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
