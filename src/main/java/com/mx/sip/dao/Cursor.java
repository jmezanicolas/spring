package com.mx.sip.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Objeto para manejar la paginación de elementos en la respuesta
 * 
 * @author rnegrete
 *
 */
public class Cursor {

	private Long currentPage;
	private Long lastPage;
	private Long nextPage;
	private Long totalPages;
	private Long totalItems;
	
	private static final ObjectMapper mapper = new ObjectMapper();
	private static final Logger log = LogManager.getLogger(Cursor.class);
	
	/**
	 * Genera un nuevo cursor de paginación a partir de el número de elementos
	 * actuales en la lista, conteo total de registros y máximos elementos por
	 * página.
	 * 
	 * @param currentRows
	 *            Número actual de registros en la lista
	 * @param rowCount
	 *            Total de elementos en la consulta
	 * @param maxRows
	 *            Elementos permitidos por página
	 * @return
	 */
	public static Cursor buildCursor(Long currentPage, Long currentRows, Long rowCount, Integer maxRows) {
		
		Cursor cursor = new Cursor();

		Long pages = calculatePages(currentRows, rowCount, maxRows);
		
		
		Long page = calculateCurrentPage(currentPage, pages);
		Long nextPage = calculateNextPage(page, pages);
		Long lastPage = calculateLastPage(page, pages);
		
		cursor.setTotalPages(pages);
		cursor.setCurrentPage(page);
		cursor.setNextPage(nextPage);
		cursor.setLastPage(lastPage);
		cursor.setTotalItems(rowCount);

		return cursor;
	}

	private static Long calculatePages(long currentRows, long rowCount, long maxRows) {
		Long pages = 0L;
		if(currentRows == 0){
			return 0L;
		}
			
		
		if (rowCount > currentRows) {
			if (rowCount > maxRows) {
				pages = rowCount / maxRows;

				if (rowCount % maxRows != 0) {
					pages++;
				}
			}
		} else {
			if (rowCount > 0) {
				pages = 1L;
			}

		}
		return pages;
	}
	
	/**
	 * Inicializa la página actual dependiendo del número de páginas
	 * @param currentPage
	 * @param totalPages
	 * @return 0 cuando totalPages es nulo o menor o igual a cero.
	 * 		   1 cuando totalPages es 1
	 * 		   >= 1 cuando totalPages es mayor a 1.
	 * Si totalPages es nulo o 
	 */
	private static Long calculateCurrentPage(Long currentPage, Long totalPages){
		Long page = 1L;
		
		if(currentPage == null){
			currentPage = 0L;
		}
		
		if(totalPages == null ){
			page = 0L;
		}
		else{
			if( totalPages <= 0){
				page = 0L;
			}
			else if( totalPages == 1) {
				page = 1L;
			}
			//totalPages es mayor a 1
			else {
				//si página actual es mayor o igual a uno se coloca el valor de acuerdo a la variable
				if(currentPage >= 1){
					if(currentPage > totalPages){
						page = totalPages;
					}
					else{
						page = currentPage;
					}					
				}
				//Sino, el valor será 1.
				else{
					page = 1L;
				}
			
			}
		}
		
		
		return page;
		
		
	}

	private static Long calculateNextPage(Long currentPage, Long totalPages) {
		Long nextPage = 1L;

		if (currentPage == null) {
			currentPage = 0L;
		}
		
		if(totalPages == null){
			totalPages = 0L;
		}

		if (totalPages <= 0) {
			nextPage = -1L;
		} 
		else {
			// totalPages mayor a currentPage
			if (totalPages > currentPage) {	
				if(currentPage <= 0){
					nextPage = -1L; 
				}
				else if( currentPage == 1 && totalPages <= 1){
					nextPage = -1L;
				}
				else{
					nextPage = currentPage + 1;
				}
			}
			else {
				if(currentPage == 1 || currentPage.equals(totalPages)){
					nextPage = -1L;
				}
				else if(currentPage > totalPages){
					nextPage = totalPages;
				}
				else{
					nextPage = totalPages;
				}
			}

		}
		return nextPage;
	}

	private static Long calculateLastPage(Long currentPage, Long totalPages) {

		Long lastPage = 0L;

		if (currentPage == null) {
			currentPage = 0L;
		}
		
		if(totalPages == null){
			totalPages = 0L;
		}

		if (totalPages <= 0) {
			lastPage = -1L;
		} 
		else {
			// currentPage mayor a 1
			if (currentPage > 1) {
				lastPage = currentPage - 1 ;
			}
			else{
				lastPage = -1L;
			}

		}
		return lastPage;
	}

	/**
	 * @return the currentPage
	 */
	public Long getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage the currentPage to set
	 */
	public void setCurrentPage(Long currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * @return the lastPage
	 */
	public Long getLastPage() {
		return lastPage;
	}

	/**
	 * @param lastPage the lastPage to set
	 */
	public void setLastPage(Long lastPage) {
		this.lastPage = lastPage;
	}

	/**
	 * @return the nextPage
	 */
	public Long getNextPage() {
		return nextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Long nextPage) {
		this.nextPage = nextPage;
	}

	
	@JsonIgnore
	public Long getTotalItems() {
		return totalItems;
	}
	
	protected void setTotalItems(Long totalRows) {
		this.totalItems = totalRows;
	}

	
	/**
	 * @return the totalPages
	 */
	public Long getTotalPages() {
		return totalPages;
	}

	
	/**
	 * @param totalPages the totalPages to set
	 */
	public void setTotalPages(Long totalPages) {
		this.totalPages = totalPages;
	}

	/**
	 * Imprime el objeto en una cadena con formato JSON
	 */
	@Override
	public String toString() {
		String str = null;
		try {
			str = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			log.error(e);
		}
		
		return str;
	}
	
	

}
