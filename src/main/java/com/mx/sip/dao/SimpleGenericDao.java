package com.mx.sip.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SimpleGenericDao<T> implements GenericDAO<T> {

	private Class<T> clazz;

	public SimpleGenericDao() {

	}

	public SimpleGenericDao(Class<T> clazz) {
		this.clazz = clazz;
	}

	public SimpleGenericDao(Class<T> clazz, SessionFactory sessionFactory) {
		this(clazz);
		this.sessionFactory = sessionFactory;
	}

	private static final Logger log = LogManager.getLogger(SimpleGenericDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Class<T> getClazz() {
		return clazz;
	}

	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Override
	@Transactional
	public String save(T object) {
		String id = null;
		Session session = null;
		log.info("Se creará entidad de tipo: {}", object.getClass());
		try {
			session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			id = (String) session.save(object);
			tx.commit();

		} catch (HibernateException e) {
			log.error("No se logró crear el registro", e);
			throw e;
		} finally {
			session.close();
		}
		return id;

	}

	@Override
	@Transactional
	public boolean update(T object) {
		boolean updated = false;
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.update(object);
			tx.commit();
			updated = true;
		} catch (HibernateException e) {
			log.error("No se logró actualizar la entidad", e);
			return false;
		} finally {
			session.close();
		}
		return updated;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public boolean delete(String id) {
		boolean deleted = false;
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			T object = (T) session.load(clazz, id);
			session.delete(object);
			tx.commit();
			object = (T) session.get(clazz, id);
			if (object == null) {
				deleted = true;
			}
		} catch (HibernateException e) {
			log.error("Falló al intentar eliminar", e);
			return false;
		} finally {
			session.close();
		}
		return deleted;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findId(String id) {
		T type = null;
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			type = (T) session.get(clazz, id);
		} catch (HibernateException e) {
			log.error("No se logró encontrar la entidad", e);
			return null;
		} finally {
			session.close();
		}
		return type;
	}

	// @SuppressWarnings({ "rawtypes" })
	// @Override
	// @Transactional
	// public ResponseList<T> list(Map constraints) {
	// return list(constraints);
	// }

	@Transactional
	public ResponseList<T> list() {
		return list(new HashMap<>());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional
	public ResponseList<T> list(Map constraints) {
		ResponseList<T> list = null;
		Session session = null;
		session = this.sessionFactory.openSession();

		try {
			long rows = getRowCount(clazz, constraints, session);
			Criteria crit = session.createCriteria(clazz);
			addConditions(crit, constraints);
			crit.setFirstResult(0);
			crit.setMaxResults((int) rows);
			list = new ResponseList<T>(crit.list());

			Cursor cursor = Cursor.buildCursor((long) 0, (long) list.size(), rows, (int)rows);
			list.setCursor(cursor);

		} catch (HibernateException e) {
			log.error("No se logró ejecutar la consulta", e);
		} catch (Exception e) {
			log.error("No se logró ejecutar la consulta", e);
		} finally {
			session.close();
		}
		return list;
	}

	public <X> List<X> customSQLSelect(String sqlQuery, Map<String, Object> values, Class<X> clazzType) {
		List<X> results = null;
		Session session = null;
		Query query = null;
		try {
			session = this.sessionFactory.openSession();
			query = session.createSQLQuery(sqlQuery);
			if (values != null && values.size() > 0) {
				for (Map.Entry<String, Object> entry : values.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}
			}
			results = query.list();
		} catch (HibernateException e) {
			log.error("Falló al intentar ejecutar la consulta {}", query.getQueryString(), e);
			return null;
		} finally {
			session.close();
		}

		return results;
	}

}
