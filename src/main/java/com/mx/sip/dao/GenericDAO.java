package com.mx.sip.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


public interface GenericDAO<T> {

	static final Logger log = LogManager.getLogger(GenericDAO.class);

	public String save(T p);
	
	public boolean update(T t);

	public boolean delete(String id);

	public T findId(String id);

	public ResponseList<T> list( Map constraints);
	  

	default long getRowCount(Class<?> clazz, Map<String, ? extends Object> constraints, Session session) {
		long count = 0;

		Criteria criteria = session.createCriteria(clazz);
		criteria.setProjection(Projections.rowCount());
		addConditions(criteria, constraints);
		count = (long) criteria.uniqueResult();

		return count;
	}

	default Integer getResults(Class<?> clazz, Map<String, ? extends Object> constraints, Session session) {
		Integer count = 0;

		Criteria criteria = session.createCriteria(clazz);
		addConditions(criteria, constraints);

		return count;
	}

	default Criteria addConditions(Criteria criteria, Map<String, ? extends Object> constraints) {
		if (constraints != null && constraints.size() > 0) {
			log.debug("Se agregan condiciones a la consulta");
			for (Iterator<String> it = constraints.keySet().iterator(); it.hasNext();) {
				String key = it.next();
				Object aux = constraints.get(key);
				Object value = aux;
				criteria.add(Restrictions.eqOrIsNull(key, value));
			}
		}
		return criteria;
	}

	default Criteria addConditions(Criteria criteria, List<Criterion> constraints) {
		if (constraints != null && constraints.size() > 0) {
			log.debug("Se agregan condiciones a la consulta");
			for (Criterion c : constraints) {
				criteria.add(c);
			}
		}
		return criteria;
	}
}
