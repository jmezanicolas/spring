package com.mx.sip.dao;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Lista de objetos paginables.
 * @author rnegrete
 * @param <T>
 *
 */
public class ResponseList<T> extends LinkedList<T>{
		
	private static final long serialVersionUID = 6044745063466479656L;
	private static final ObjectMapper mapper = new ObjectMapper();
	private static final Logger log = LogManager.getLogger(ResponseList.class);
	
	
	public ResponseList(){
		cursor = new Cursor();
		cursor.setCurrentPage(-1L);
		cursor.setLastPage(-1L);
		cursor.setNextPage(-1L);
		cursor.setTotalPages(0L);
	}
	
	
	public ResponseList(List<T> list){
		this();
		this.addAll(list);
	}
	
	
	public ResponseList(Collection<? extends T> c) {
		super(c);
	}



	private Cursor cursor = null;
	
	public Cursor getCursor() {
		return cursor;
	}
	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {				
		String str = null;
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		ArrayNode array = JsonNodeFactory.instance.arrayNode();
		String typeName = "results";
		String cursorName ="cursor";
		
		if(this.size() > 0){						
			for(Iterator<T> it = this.iterator(); it.hasNext(); ) {
				T type = it.next();		
				array.add(stringToJson(type.toString()));
			}
		}
		node.set(typeName, array);
		node.set(cursorName, stringToJson(cursor.toString()));
		try {
			str = mapper.writeValueAsString(node);
			log.debug(str);
		} catch (JsonProcessingException e) {
			log.error(e);
		}
			
		return str;
	}
	
	
	private JsonNode stringToJson(String jsonString){
		JsonNode node = null;
		try {
			node = mapper.readTree(jsonString);
		} catch (IOException e1) {
			log.error(e1);
		}
		return node;
	}
	
}
