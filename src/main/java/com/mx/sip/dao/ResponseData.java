package com.mx.sip.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mx.sip.utils.Returnable;

public class ResponseData<I extends Returnable> {

	private static final Logger log = LogManager.getLogger(ResponseData.class);

	private static final ObjectMapper mapper = new ObjectMapper();

	private List<I> data = null;

	private Page paging;

	public ResponseData() {
		data = new ArrayList<I>(0);
		paging = new Page();
	}

	public ResponseData(Collection<? extends I> data) {
		this();
		this.data.addAll(data);
	}

	public ResponseData(Collection<? extends I> data, Cursor cursor) {
		this();
		this.data.addAll(data);
		paging.setNextPage(cursor.getNextPage().intValue());
		paging.setPreviousPage(cursor.getLastPage().intValue());
		paging.setMax(cursor.getTotalPages().intValue());
		paging.setTotalItems(cursor.getTotalItems());

	}

	public ResponseData(ResponseList<? extends I> list) {
		this(list, list.getCursor());
	}

	/**
	 * @return the data
	 */
	public List<? extends Returnable> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<I> data) {
		this.data = data;
	}

	/**
	 * @return the paging
	 */
	public Page getPaging() {
		return paging;
	}

	/**
	 * @param paging
	 *            the paging to set
	 */
	public void setPaging(Page paging) {
		this.paging = paging;
	}

	public void addData(I item) {
		this.data.add(item);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = null;
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		ArrayNode array = JsonNodeFactory.instance.arrayNode();
		String typeName = "data";
		String cursorName = "paging";

		if (data != null && data.size() > 0) {
			for (Iterator<? extends Returnable> it = data.iterator(); it.hasNext();) {
				Returnable object = it.next();
				array.add(stringToJson(object.toJsonString()));
			}
		}
		node.set(typeName, array);
		node.set(cursorName, stringToJson(paging.toString()));
		try {
			str = mapper.writeValueAsString(node);
			log.debug(str);
		} catch (JsonProcessingException e) {
			log.error(e);
		}
		return str;
	}

	private JsonNode stringToJson(String jsonString) {
		JsonNode node = null;
		try {

			node = mapper.readTree(jsonString);
		} catch (IOException e1) {
			log.error(e1);
		}
		return node;
	}

}
