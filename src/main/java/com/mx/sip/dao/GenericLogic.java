package com.mx.sip.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.mx.sip.utils.Returnable;

public interface GenericLogic<T> {
	T findId(String id);

	String create(T object);

	boolean update(T object);

	boolean delete(String id);

	ResponseData<Returnable> list(Map<String, Object> params);

	default Map<String, ? extends Object> extractConstraints(String filters, final Set<String> validFilters) {
		Map<String, ? extends Object> list = null;
		if (filters == null) {
			return null;
		} else {

			final Set<String> _auxFilters;
			if (validFilters == null) {
				_auxFilters = new HashSet<String>();
			} else {
				_auxFilters = validFilters;
			}
			// Parte el query string en pares
			list = Arrays.asList(filters.split("&")).stream()
					// Vuelve a dividir en llave y valor
					.map(s -> s.split("="))
					// filtra solo aquellos de longitud 2 y que el nombre exista
					// en la listra de filtros válidos
					.filter(s -> s.length == 2 && _auxFilters.contains(s[0]))
					// Agrega los seleccionados en el mapa
					.collect(Collectors.toMap(e -> e[0], e -> e[1]));

			list.values().stream().forEach(System.out::println);

		}
		return list;
	}

}
